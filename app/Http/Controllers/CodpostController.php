<?php

namespace App\Http\Controllers;

use App\codpost;
use Illuminate\Http\Request;

class CodpostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $codpostal = $request->codpostal;
        $linea2 = "";

        $arrDatos = array
        (
            "zip_code"		=> 0,	
            "locality"		=> '',
            "federal_entity" => '',
            "settlements" => '',
            "municipality" => ''
        );
		
		 $arrDatos2 = array
        (
            "arrDatos" => ''
        );

        $federal_entity = array
        (
            "key" => 0,
            "name" => '',
            "code" =>''
        );

        $settlements = array
        (
            "key" => 0,
            "name" => '',
            "zone_type" =>'',
            "settlement_type" => ''
        );

        $settlement_type = array(
            "name" => ''
        );
        
        $municipality = array(
            "key" => 0,
            "name" => ''
        );
        $cont = 0;
   
        foreach(file(dirname(__FILE__).'/CPdescarga.txt') as $line) {
                
            $arrinfcodpost = explode("|", $line);
            if($arrinfcodpost[0] == $codpostal){
                 $federal_entity["key"] 	= (int)$arrinfcodpost[7];
                $federal_entity["name"] 	= utf8_encode($arrinfcodpost[4]);

                $settlements["key"] 	= (int)$arrinfcodpost[12];
                $settlements["name"] 	= utf8_encode($arrinfcodpost[1]);
                $settlements["zone_type"] 	= utf8_encode($arrinfcodpost[13]);
            
               
                $settlement_type["name"] 	= utf8_encode($arrinfcodpost[2]);
                
                $settlements['settlement_type'] = $settlement_type;
                $municipality["key"] 	= (int)$arrinfcodpost[11];
                $municipality["name"] 	= utf8_encode($arrinfcodpost[3]);

                $arrDatos["zip_code"] 	= utf8_encode($arrinfcodpost[0]);
                $arrDatos["locality"] 	= utf8_encode($arrinfcodpost[5]);


                $arrDatos["federal_entity"] 	= $federal_entity;
                $arrDatos["settlements"] 	= 		$settlements;
                $arrDatos["municipality"] 	= 		$municipality;
                
                array_push($arrDatos2, $arrDatos);
                
            }
        }
        
        if (empty($arrDatos["zip_code"])) {
            $arrDatos["zip_code"] = 'No Existe Información para zip_code ' .$codpostal;
            return $arrDatos;
        }
        else
        {
            return  $arrDatos2;
        }
        
          //  01210|Santa Fe|Pueblo|Álvaro Obregón|Ciudad de México|Ciudad de México|01401|09|01401||28|010|0082|Urbano|01
            //01219|Lomas de Santa Fe|Colonia|Álvaro Obregón|Ciudad de México|Ciudad de México|01401|09|01401||09|010|0084|Urbano|01
          
        //File::get(storage_path('CPdescarga.txt'));
     }
    public function store(Request $request)
    {
        //
        
         
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\codpost  $codpost
     * @return \Illuminate\Http\Response
     */
    public function show(codpost $codpost)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\codpost  $codpost
     * @return \Illuminate\Http\Response
     */
    public function edit(codpost $codpost)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\codpost  $codpost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, codpost $codpost)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\codpost  $codpost
     * @return \Illuminate\Http\Response
     */
    public function destroy(codpost $codpost)
    {
        //
    }
}
